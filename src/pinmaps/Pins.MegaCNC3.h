// -------------------------------------------------------------------------------------------------
// Pin map for CNC 3.0 Shield on Mega2560

#if defined(__AVR_ATmega2560__)

// Misc. pins
#define DS3234_CS_PIN      	10     // Default CS Pin for DS3234 on SPI

// The PEC index sense is a 5V logic input, resets the PEC index on rising edge then waits for 60 seconds before allowing another reset
#define PecPin                	11
#define AnalogPecPin          	1

// The limit switch sense is a 5V logic input which uses the internal (or external 2k) pull up, shorted to ground it stops gotos/tracking
#define LimitPin              	A0

// The status LED is a two wire jumper with a 10k resistor in series to limit the current to the LED
//                                           Atmel 2560 port/bit
#define LEDposPin             	12     // LED   PH5
#define LEDnegPin             	13     // GND   PH6
#define LEDneg2Pin           	9     // PGND  PB4
#define ReticlePin           	44     // PGND

// For a piezo buzzer
#define TonePin              	42     // Tone

// The PPS pin is a 5V logic input, OnStep measures time between rising edges and adjusts the internal sidereal clock frequency
#define PpsPin               	21     // Interrupt 2 on Pin 21 (alternate Int3 on Pin20)

// Axis1 RA/Azm step/dir driver
#define Axis1_EN             	8     // Enable
#define Axis1_STEP           	2     // Step PB7
#define Axis1_StepPORT    	PORTE     //
#define Axis1_StepBIT         	4     //
#define Axis1_DIR            	5     // Dir  PB5
#define Axis1_DirPORT     	PORTE     //
#define Axis1_DirBIT          	3     //

// Axis2 Dec/Alt step/dir driver
#define Axis2_EN             	8     // Enable
#define Axis2_STEP            	3     // Step  PH3
#define Axis2_StepPORT    	PORTE     //
#define Axis2_StepBIT         	5     //
#define Axis2_DIR             	6     // Dir   PG5
#define Axis2_DirPORT     	PORTH     //
#define Axis2_DirBIT          	3     //

/*

// Pins to rotator stepper driver
#define Axis3_EN             -1     // Enable
#define Axis3_STEP           A9     // Step
#define Axis3_DIR            A8     // Dir

// Pins to focuser1 stepper driver
#define Axis3_EN             -1     // Enable
#define Axis4_STEP          A11     // Step
#define Axis4_DIR           A10     // Dir

// Pins to focuser2 stepper driver
#define Axis3_EN             -1     // Enable
#define Axis5_STEP          A13     // Step
#define Axis5_DIR           A12     // Dir

*/

// ST4 interface
#define ST4RAw               47     // ST4 RA- West
#define ST4DEs               43     // ST4 DE- South
#define ST4DEn               45     // ST4 DE+ North
#define ST4RAe               49     // ST4 RA+ East


#else
#error "Wrong processor for this configuration!"

#endif
